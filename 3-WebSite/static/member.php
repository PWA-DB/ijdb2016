<?php
/**
 * Membership
 *
 * Membership information and so forth
 *
 * @file:       /static/membership.php
 * @project:    IJDB2016
 * @author:     Adrian Gould <Adrian.Gould@smtafe.wa.edu.au>
 * @date:       21/05/2016 12:47 PM
 * @version:    1.0
 * @copyright:  Copyright 2016 TAFE OpenSource,
 *              Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0    21/05/2016
 *          Initial version
 */
?>

<div class="row">
    <div class="col-xs-12">
        <div class="page-header">
            <h1>Internet Joke Database
                <small>Membership</small>
            </h1>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h2>Membership details and link to forms</h2>
    </div>
</div>
