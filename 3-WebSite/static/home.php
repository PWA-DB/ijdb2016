<?php
/**
 * Home Page
 *
 * Site Home Page
 *
 * @file:       /static/home.php
 * @project:    IJDB2016
 * @author:     Adrian Gould <Adrian.Gould@smtafe.wa.edu.au>
 * @date:       21/05/2016 12:21 PM
 * @version:    1.0
 * @copyright:  Copyright 2015 TAFE OpenSource,
 *              Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0    21/05/2016
 *          Initial version
 */

try {
    $resultsJ = false;
    /*** Query the database list only the last five added jokes ***/
    $sql = 'SELECT * FROM joke';
    /*** prepare SQL in DB engine ***/
    $stmt = $pdo->prepare($sql);
    /*** Execute the query ***/
    $stmt->execute();
    /*** get results ***/
    $resultsJ = $stmt->fetchAll(PDO::FETCH_OBJ);
    $jokeCount = $stmt->rowCount();

} catch (PDOException $e) {
    $error['general'] = "Error fetching joke count:<br>" . $e->getMessage();
    include './error.html.php';
    exit();
} // end try ... catch ... list jokes

try {
    $resultsA = false;
    /*** Query the database list only the last five added jokes ***/
    $sql = 'SELECT * FROM author';
    /*** prepare SQL in DB engine ***/
    $stmt = $pdo->prepare($sql);
    /*** Execute the query ***/
    $stmt->execute();
    /*** get results ***/
    $resultsA = $stmt->fetchAll(PDO::FETCH_OBJ);
    $authorCount = $stmt->rowCount();

} catch (PDOException $e) {
    $error['general'] = "Error fetching author count:<br>" . $e->getMessage();
    include './error.html.php';
    exit();
} // end try ... catch ... list jokes

try {
    $resultsRandomJ = false;
    /*** Query the database list only the last five added jokes ***/
    $sql = 'SELECT * FROM joke ORDER BY 1*(UNIX_TIMESTAMP() ^ id) & 0xffff LIMIT 1';

    /*** prepare SQL in DB engine ***/
    $stmt = $pdo->prepare($sql);
    /*** Execute the query ***/
    $stmt->execute();
    /*** get results ***/
    $resultsRandomJ = $stmt->fetch(PDO::FETCH_OBJ);

} catch (PDOException $e) {
    $error['general'] = "Error fetching author count:<br>" . $e->getMessage();
    include './error.html.php';
    exit();
} // end try ... catch ... list jokes

?>

<div class="row">
    <div class="col-xs-12">
        <div class="page-header">
            <h1>Internet Joke Database
                <small>Home</small>
            </h1>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-6">
        <h2>Random Joke</h2>
        <p><?= $resultsRandomJ->joketext; ?></p>
    </div>

    <div class="col-xs-12 col-sm-6">
        <div class="row">
            <div class="col-xs-6">
                <div class="panel panel-default text-center">
                    <div class="panel-heading">Jokes</div>
                    <div class="panel-body bg-success">
                        <p class="bigNum"><?= $jokeCount; ?></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="panel panel-default text-center">
                    <div class="panel-heading">Authors</div>
                    <div class="panel-body bg-danger">
                        <p class="bigNum"><?= $authorCount; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
