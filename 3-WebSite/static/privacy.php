<?php
/**
 * Privacy Policy
 *
 * Contains the site privacy policy
 *
 * @file:       /static/privacy.php
 * @project:    IJDB2016
 * @author:     Adrian Gould <Adrian.Gould@smtafe.wa.edu.au>
 * @date:       21/05/2016 12:47 PM
 * @version:    1.0
 * @copyright:  Copyright 2016 TAFE OpenSource,
 *              Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0    21/05/2016
 *          Initial version
 */
?>

<div class="row">
    <div class="col-xs-12">
        <div class="page-header">
            <h1>Internet Joke Database
                <small>Privacy Policy</small>
            </h1>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h2>Privacy Policy Details below</h2>
    </div>
</div>
