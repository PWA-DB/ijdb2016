<?php
/**
 * Privacy Policy
 *
 * Contains the site privacy policy
 *
 * @File:       /static/privacy.php
 * @Project:    IJDB2016
 * @Author:     Adrian Gould <Adrian.Gould@smtafe.wa.edu.au>
 * @Date:       21/05/2016 12:47 PM
 * @Version:    1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 *              Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0    21/05/2016
 *          Initial version
 */
?>

<div class="row">
    <div class="col-xs-12">
        <div class="page-header">
            <h1>Internet Joke Database
                <small>About</small>
            </h1>
        </div>
    </div>
</div>
