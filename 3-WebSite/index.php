<?php
/**
 * IJDB Controller File
 *
 * @File:       /index.php
 * @Project:    IJDB2016
 * @Author:     Adrian Gould <Adrian.Gould@smtafe.wa.edu.au>
 * @Date:       19/05/2016 12:47 PM
 * @Version:    1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 *              Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0    19/05/2016
 *          Initial version
 */

require_once 'db/functions.php';

require_once 'db/connect.php';

require_once 'templates/siteHeader.php';

/*** Controller ***/
if (isset($_GET["action"])) {

    // ensure the action is lowercase
    $action = strtolower($_GET['action']);

    switch ($action) {

        /*** Joke Actions ***/
        case 'jokes-add': {
            $pageTitle = "Add Jokes";
            include 'jokes/add.php';
            break;
        }
        case 'jokes-list' : {
            $pageTitle = "List";
            include 'jokes/retrieve.php';
            break;
        }
        case 'jokes-edit': {
            $pageTitle = "Edit Jokes";
            include 'jokes/edit.php';
            break;
        }
        case 'jokes-delete': {
            $pageTitle = "Delete Jokes";
            include 'jokes/delete.php';
            break;
        }

        /*** Main Site Actions ***/
        case "privacy": {
            $pageTitle = "Privacy Policy";
            include "static/privacy.php";
            break;
        }
        case "about": {
            $pageTitle = "About";
            include "static/about.php";
            break;
        }
        case "member": {
            $pageTitle = "Membership";
            include "static/member.php";
            break;
        }
        default: {
            $pageTitle = "";
            include "static/home.php";
        }
    } // end switch

} // end if

require_once 'templates/siteFooter.php';
