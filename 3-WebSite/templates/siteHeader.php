<?php
/**
 * Site Header HTML "Template" with default navigation
 *
 * @file:       /templates/siteHeader.php
 * @project:    IJDB2016
 * @author:     Adrian Gould <Adrian.Gould@smtafe.wa.edu.au>
 * @date:       21/05/2016 12:47 PM
 * @version:    1.0
 * @copyright:  Copyright 2016 TAFE OpenSource,
 *              Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0    21/05/2016
 *          Initial version
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="./favicon.ico">

    <?php
    /**
     * If we do NOT have a page title then set the page title to
     * blank, otherwise prepend it with a | before we output it to the HTML.
     */
    if (!isset($pageTitle)) {
        $pageTitle = "";
    } else {
        $pageTitle = " | " . $pageTitle;
    }
    ?>
    <title>Internet Joke DataBase <?= $pageTitle; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">

    <!-- FontAwesome and creative commons fonts -->
    <link href="./css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/cc-icons.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="./css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements
         and media queries -->
    <!--[if lt IE 9]>
    <script
        src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script
        src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="?action=home">IJDB</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="?action=home">Home</a></li>
                <li><a href="?action=contact">Contact</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                       role="button" aria-haspopup="true"
                       aria-expanded="false">Jokes <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="?action=jokes-list">List</a></li>
                        <li role="separator" class="divider"></li>
                        <li class="dropdown-header">Members Only</li>
                        <li><a href="?action=jokes-add">Add</a></li>
                        <li><a href="?action=jokes-edit">Edit</a></li>
                        <li><a href="?action=jokes-delete">Delete</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                       role="button" aria-haspopup="true"
                       aria-expanded="false">Members <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="?action=member-login">Login</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="?action=member-profile">Profile</a></li>
                    </ul>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="container">
