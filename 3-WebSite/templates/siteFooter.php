<?php
/**
 * Default Site Footer
 *
 * @file:       /templates/siteFooter.php
 * @project:    IJDB2016
 * @author:     Adrian Gould <Adrian.Gould@smtafe.wa.edu.au>
 * @date:       19/05/2016
 * @version:    1.0 Original site footer with bootstrap
 * @copyright:  Copyright 2015 TAFE OpenSource,
 *              Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0    19/05/2016
 *          Initial version
 */
?>
<!-- Site Footer -->
<footer class="siteFooter">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <ul class="list-inline">
                    <li>
                        <div class="row col-xs-2"><a rel="license"
                                                     href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
                                <i class="cc cc-by-nc-sa cc-lg"></i>

                            </a></div>
                        <div class="col-xs-9">
                            <span xmlns:dct="http://purl.org/dc/terms/"
                                  property="dct:title">IJDB</span> by <a
                                xmlns:cc="http://creativecommons.org/ns#"
                                href="http://tafeopensource.org.au"
                                property="cc:attributionName"
                                rel="cc:attributionURL">TAFE OpenSource &
                                Adrian Gould</a> is licensed under a <a
                                rel="license"
                                href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative
                                Commons Attribution-NonCommercial-ShareAlike
                                4.0 International License</a>.<br/>Based on a
                            work at <a xmlns:dct="http://purl.org/dc/terms/"
                                       href="https://Ady_Gould@bitbucket.org/PWA-DB/ijdb2016.git"
                                       rel="dct:source">https://Ady_Gould@bitbucket.org/PWA-DB/ijdb2016.git</a>.
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-4">
                <ul class="list-inline">
                    <li><a href="?action=home">Home</a></li>
                    <li><a href="?action=jokes-list">Jokes</a></li>
                    <li><a href="?action=about">About</a></li>
                    <li><a href="?action=member">Membership</a></li>
                    <li><a href="?action=privacy">Privacy</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

</div><!-- end container (opened in siteheader)-->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="./js/jquery-1.12.3.min.js"></script>
<script>window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"><\/script>')</script>
<script src="./js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="./js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
