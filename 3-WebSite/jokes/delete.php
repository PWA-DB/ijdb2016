<?php
/**
 * Joke Delete
 *
 * @file:       /jokes/delete.php
 * @project:    IJDB2016
 * @author:     Adrian Gould <Adrian.Gould@smtafe.wa.edu.au>
 * @date:       21/05/2016 3:03 PM
 * @version:    1.0
 * @copyright:  Copyright 2016 TAFE OpenSource,
 *              Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0    21/05/2016
 *          Initial version
 */


if (isset($_POST) && isset($_POST['delete'])) {

    echo "DELETE";

    var_dump($_POST);

    if ($_POST['jokeID'] > '') {
        $jokeID = $_POST['jokeID'];

        // check if the id exists
        try {
            $results = false;
            /*** Query the database to find the bookmarks ***/
            $sql = 'SELECT * FROM joke where id = :jokeID';
            /*** prepare SQL in DB engine ***/
            $stmt = $pdo->prepare($sql);
            $stmt->bindParam(':jokeID', $jokeID);
            /*** Execute the query ***/
            $stmt->execute();
            /*** get results ***/
            $results = $stmt->fetchAll(PDO::FETCH_OBJ);
            $numJokes = $stmt->rowCount();

        } catch (PDOException $e) {
            $error[] = ["" => "Error fetching jokes:" . $e->getMessage()];
            include '../error.html.php';
            exit();
        }
        //
        // if id submitted exists, display joke being removed
        if ($numJokes > 0) {
            // create SQL to delete the record
            $sql = 'DELETE FROM joke where id = :jokeID';

            /*** prepare SQL in DB engine ***/
            $stmt = $pdo->prepare($sql);
            $stmt->bindParam(':jokeID', $jokeID);
            /*** Execute the query ***/
            $stmt->execute();
            /*** get results ***/
            $results = $stmt->fetchAll(PDO::FETCH_OBJ);

            // if deleted ok, display "success" message
            if ($results != false) {
                echo "SUCCESS";
            }
            // else display, "error when deleting" message

        } // end if numjokes

    } else {

    } // end if jokeID

} // end isset POST

try {
    $results = false;
    /*** Query the database to find the bookmarks ***/
    $sql = 'SELECT * FROM joke';
    /*** prepare SQL in DB engine ***/
    $stmt = $pdo->prepare($sql);
    /*** Execute the query ***/
    $stmt->execute();
    /*** get results ***/
    $results = $stmt->fetchAll(PDO::FETCH_OBJ);
    $numJokes = $stmt->rowCount();

} catch (PDOException $e) {
    $error[] = ["" => "Error fetching jokes:" . $e->getMessage()];
    include '../error.html.php';
    exit();
}

?>
    <div class="row">
        <div class="col-xs-12">
            <div class="page-header">
                <h1>Internet Joke Database
                    <small>Jokes</small>
                </h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Our Jokes</h3>
                </div>

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Joke</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    foreach ($results as $row) {
                        ?>
                        <tr>
                            <td><?= $row->id; ?>
                            <td><?= $row->joketext; ?></td>
                            <td>
                                <form action="?action=jokes-delete"
                                      class="form-inline" method="POST">
                                    <input type="hidden" name="jokeID"
                                           value="<?= $row->id; ?>">
                                    <button name="delete"
                                            value="submit"
                                            class="btn btn-danger">
                                        <i class="fa fa-remove"></i>
                                        Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>

                <div class="panel-footer">We have <?= $numJokes; ?>
                    jokes
                </div>
            </div>
        </div>
    </div>
<?php

