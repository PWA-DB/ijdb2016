<?php
/**
 * Joke Home Page
 *
 * @file:       /jokes/index.php
 * @project:    IJDB2016
 * @author:     Adrian Gould <Adrian.Gould@smtafe.wa.edu.au>
 * @date:       21/05/2016 3:02 PM
 * @version:    1.0
 * @copyright:  Copyright 2016 TAFE OpenSource,
 *              Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0    21/05/2016
 *          Initial version
 */

try {
    $results = false;
    /*** Query the database to find the requested bookmark ***/
    $sql = 'SELECT * FROM joke';
    /*** prepare SQL in DB engine ***/
    $stmt = $pdo->prepare($sql);
    /*** Execute the query ***/
    $stmt->execute();
    /*** get results ***/
    $results = $stmt->fetchAll(PDO::FETCH_OBJ);
    $numJokes = $stmt->rowCount();

} catch (PDOException $e) {
    $error[] = "Error fetching jokes:<br>" . $e->getMessage();
    include './error.html.php';
    exit();
}
?>
    <div class="row">
        <div class="col-xs-12">
            <div class="page-header">
                <h1>Internet Joke Database
                    <small>Jokes</small>
                </h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Our Jokes</h3>
                </div>

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Joke</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    foreach ($results as $row) {
                        ?>
                        <tr>
                            <td><?= $row->id; ?></td>
                            <td><?= $row->joketext; ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>

                <div class="panel-footer">We have <?= $numJokes; ?>
                    jokes</div>
            </div>
        </div>
    </div>
<?php
