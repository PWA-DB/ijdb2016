<?php
/**
 * Add Joke
 *
 * @File:       /jokes/add.php
 * @Project:    IJDB2016
 * @Author:     Adrian Gould <Adrian.Gould@smtafe.wa.edu.au>
 * @Date:       21/05/2016 3:02 PM
 * @Version:    1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 *              Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0    21/05/2016
 *          Initial version
 */

//reset the error array
$error = array();

// Check to see if the form was posted and was submitted
if (isset($_POST) && isset($_POST['jokeAdd'])) {

    // initialise the results, joke and author variables
    $results = false;
    $jokeAuthor = 0;
    $jokeText = '';

    // If we have an author, store, otherwise set an error message
    if (isset($_POST['jokeAuthor']) && $_POST['jokeAuthor'] > 0) {
        $jokeAuthor = intval($_POST['jokeAuthor']);
    } else {
        $error['author'] = 'Author Missing';
    }

    // If we have a joke, store, otherwise set an error message
    if (isset($_POST['jokeText']) && $_POST['jokeText'] > '') {
        $jokeText = $_POST['jokeText'];
    } else {
        $error['joke'] = 'Joke Missing';
    }

    // if both the joke and author are set then try to insert the joke
    // into the database
    if ($jokeText > '' && $jokeAuthor > 0) {
        $jokeAuthor = intval($jokeAuthor);
        try {

            /*** Create the basic SQL INSERT ***/
            $sql = '
                INSERT INTO joke(joketext, authorid, jokedate) 
                VALUES (:jokeText, :jokeAuthor, CURRENT_TIMESTAMP);';

            /*** prepare SQL in DB engine ***/
            $stmt = $pdo->prepare($sql);

            /*** bind to the placeholders ***/
            $stmt->bindParam(':jokeText', $jokeText, PDO::PARAM_STR);
            $stmt->bindParam(':jokeAuthor', $jokeAuthor, PDO::PARAM_INT);

            /*** Execute the query ***/
            $stmt->execute();

            /*** get results ***/
            $results = $stmt->fetchAll(PDO::FETCH_OBJ);

            /*** Reset the data ***/
            $jokeAuthor = '';
            $jokeText = '';

        } catch (PDOException $e) {
            $error['general'] = "Error adding joke:<br>" . $e->getMessage();
            include './error.html.php';
            exit();
        } // end try insert the data

    } // end if

} else {
    // make sure the joke and author are reset
    $jokeText = '';
    $jokeAuthor = 0;
}// end if posted

// attempt to retrieve the last five added jokes
try {
    $results = false;
    /*** Query the database list only the last five added jokes ***/
    $sql = 'SELECT * FROM joke 
            ORDER BY id DESC 
            LIMIT 5';
    /*** prepare SQL in DB engine ***/
    $stmt = $pdo->prepare($sql);
    /*** Execute the query ***/
    $stmt->execute();
    /*** get results ***/
    $results = $stmt->fetchAll(PDO::FETCH_OBJ);

} catch (PDOException $e) {
    $error['general'] = "Error fetching jokes:<br>" . $e->getMessage();
    include './error.html.php';
    exit();
} // end try ... catch ... list jokes


?>

    <div class="row">
        <div class="col-xs-12">
            <div class="page-header">
                <h1>Internet Joke Database
                    <small>Add</small>
                </h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Add Jokes</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php if (sizeof($error) > 0) : // display any error messages ?>
                                <ul class="list-group">
                                    <li class="list-group-item list-group-item-heading">
                                        <strong>You have errors</strong>
                                    </li>
                                    <?php foreach ($error as $key => $errorMessage): ?>
                                        <li class="list-group-item list-group-item-danger"><?= $errorMessage ?> </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; // end if error messages ?>
                        </div>
                    </div>
                    <form class="form-horizontal" method="post"
                          action="?action=jokes-add">
                        <div
                            class="form-group <?= isset($error['joke']) ? 'has-error' : ''; ?>">
                            <label for="jokeText"
                                   class="col-sm-3 control-label">Joke Text
                            </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control"
                                       name="jokeText" id="jokeText"
                                       value="<?= $jokeText; ?>"
                                       placeholder="Gimme your joke">
                            </div>
                        </div>
                        <div
                            class="form-group <?= isset($error['author']) ? 'has-error' : ''; ?>">
                            <label for="jokeAuthor" class="col-sm-3
                                   control-label">Author</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control"
                                       id="jokeAuthor" name="jokeAuthor"
                                       value="<?= $jokeAuthor; ?>"
                                       placeholder="Author">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-info"
                                        value="submit" name="jokeAdd"
                                        id="jokeAdd">Add
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Our Jokes</h3>
                </div>

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Joke</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    if ($results) :
                        foreach ($results as $row) :
                            ?>
                            <tr>
                                <td><?= $row->id; ?></td>
                                <td><?= $row->joketext; ?></td>
                            </tr>
                            <?php
                        endforeach;
                    else:
                        ?>
                        <tr class="bg-warning">
                            <td></td>
                            <td>This is no joke, but we have no jokes .</td>
                        </tr>
                        <?php
                    endif;
                    ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
<?php
