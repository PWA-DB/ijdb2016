<?php
/**
 * Error Page
 *
 * @file:       /error.html.php
 * @project:    IJDB2016
 * @author:     Adrian Gould <Adrian.Gould@smtafe.wa.edu.au>
 * @date:       19/05/2016
 * @version:    1.0
 * @copyright:  Copyright 2016 TAFE OpenSource,
 *              Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0    19/05/2016
 *          Initial version
 */

require_once('templates/siteHeader.php');
?>


    <div class="row">
        <div class="col-xs-12">
            <div class="page-header">
                <h1>Internet Joke Database</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">

            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h2 class="panel-title">Whoops!</h2>
                </div>
                <div class="panel-body">
                    <h4>Apologies, we seem to have hit a small problem
                        &hellip;</h4>
                    <?php
                    if (isset($error) && is_array($error)){
                        foreach ($error as $key =>
                                 $item) {
                            ?>
                            <p>
                                <?= $item; ?>
                            </p>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>


        </div>
    </div>

<?php
require_once('templates/siteFooter.php');
