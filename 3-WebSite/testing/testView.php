<?php
/**
 * Testing View for site
 *
 * @Author:     Adrian Gould
 * @Date:       19/05/2016
 * @Version:    1.0 Original test view
 */

require_once('../db/connect.php');

/*** Load the Site Header **/
require_once('../templates/siteHeader.php');
?>
<!-- Main container and content -->
<div class="container">

    <!-- .row>.col-xs-6*2>p{Column $} [TAB to expand] -->

    <div class="row">
        <div class="col-xs-6">
            <p>Column 1</p>
        </div>
        <div class="col-xs-6">
            <p>Column 2</p>
        </div>
    </div>

</div><!-- end main content container -->

<?php
require_once('../templates/siteFooter.php');
