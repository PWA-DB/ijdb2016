<?php
/**
 * Testing installation of PHP
 *
 * @Author:    Adrian Gould
 * @Date:       19/05/2016
 *
 * @Version:    1.0 Simple test file (php info)
 */

echo "<h1>IJDB Test Server</h1>";
echo phpinfo();
