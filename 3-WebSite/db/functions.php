<?php
/**
 * Various utility functions and routines
 *
 * The magic quotes routine is always run when included. The other
 * functions are included and used as required.
 *
 * @File:       /db/functions.php
 * @Project:    IJDB2016
 * @Author:     Adrian Gould <Adrian.Gould@smtafe.wa.edu.au>
 * @Date:       19/05/2016 12:47 PM
 * @Version:    1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 *              Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0    19/05/2016
 *          Initial version
 */

/**
 * Magic Quotes Fix
 */
if (get_magic_quotes_gpc()) {
    $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    while (list($key, $value) = each($process)) {
        foreach ($value as $k => $v) {
            unset($process[$key][$k]);
            if (is_array($v)) {
                $process[$key][stripslashes($k)] = $v;
                $process[] = &$process[$key][stripslashes($k)];
            } else {
                $process[$key][stripslashes($k)] = stripslashes($v);
            }// end if
        } // end foreach
    }// end while
    unset($process);
} // end if

/**
 * debugging function
 *
 * pass in a variable (array, int, string) and it will be output
 *
 * @param $info
 */
function d($info){
    if (is_array($info)) {
        foreach ($info as $k => $v) {
            echo "<p>(" .$k.')'. $v . "</p>";
        } // end foreach
    } else {
        echo "<p>" . $info . "</p>";
    }
}
