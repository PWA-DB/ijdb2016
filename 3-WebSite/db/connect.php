<?php
/**
 * Database connection information
 *
 * @File:       /db/connect.php
 * @Project:    IJDB2016
 * @Author:     Adrian Gould <Adrian.Gould@smtafe.wa.edu.au>
 * @Date:       19/05/2016 12:47 PM
 * @Version:    1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 *              Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0    19/05/2016
 *          Initial version
 */

/*** Database Information ***/
$host = '127.0.0.1';        // database server address
$port = "3306";             // database server port
$db = 'ajg_ijdb';           // database name
$user = 'ajg_ijdb';         // database user name
$pass = 'password';         // database user password
$charset = 'utf8';          // database char set

// Create the Connection String
$dsn = "mysql:$host;port=$port;dbname=$db;charset=$charset";

// Set the options for PDO Interaction
$opt = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
    PDO::ATTR_EMULATE_PREPARES => false,
];

// Initialise the $pdo variable to false
$pdo = false;

// Now try connecting to the database
try {
    $pdo = new PDO($dsn, $user, $pass, $opt);
} catch (PDOException $e) {
    // set an error message and call the error page
    $error = "<p>System unavailable. Please try again later.</p>";
    $error .= "<p>" . $e->getMessage() . "</p>";
    include('error.html.php');
    exit();
}
